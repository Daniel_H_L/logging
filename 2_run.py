"""
Created on Mar 3, 2020

@author: Dherrera
"""

import unittest

# import coverage

from agendaBox.agendaModule import AgendaHandler


myAgenda = AgendaHandler()

"""
    #Unit test of AgendaHandler class
class MyTestClass(unittest.TestCase):

    def test_load_agenda(self):
        self.assertIsInstance(myAgenda.load_agenda(), AgendaHandler, "tested")


    def test_add_new_record(self):

        my_list = ["daniel,30,Mexico,daniel@hotmail.com"]
        myAgenda.add_new_record("daniel", "30", "Mexico", "daniel@hotmail.com")
        self.assertEqual(myAgenda.directory, my_list, "tested")

    def test_save_agenda(self):
        self.assertTrue(myAgenda.save_agenda(myAgenda.directory), "tested")

    def test_delete_record(self):

        self.assertTrue(myAgenda.delete_record("daniel@hotmail.com", "30"), "tested")
        return

    def test_search_record(self):

        self.assertTrue(myAgenda.search_record("daniel", "30"), "tested")

    def test_print_records(self):

        self.assertIsInstance(myAgenda.print_records(myAgenda.directory), list, "tested")
"""

if __name__ == '__main__':
    pass

  #  unittest.main()

myAgenda.load_agenda()
myAgenda.add_new_record("Daniel", "30", "Mexico", "daniel@hotmail.com")
myAgenda.delete_record("daniel@hotmail.com","30")
myAgenda.add_new_record("Rodrigo", "50", "Honduras", "Rodrigo@hotmail.com")
myAgenda.add_new_record("Paul", "33", "Belice", "Paul@hotmail.com")
myAgenda.add_new_record("jenni", "23", "Italia", "jenni@hotmail.com")
myAgenda.add_new_record("joel", "45", "grecia", "paco@hotmail.com")
myAgenda.save_agenda(myAgenda.directory)
myAgenda.print_records(myAgenda.directory)
print ( myAgenda.search_record("daniel@hotmail.com", "30") )
