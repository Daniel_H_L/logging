"""
Created on Mar 3, 2020

@author: DHerrera
"""
import logging

# logging.basicConfig(filename='logFile',level=logging.INFO,format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')

logger = logging.getLogger(__name__)  # __name__=projectA.moduleB ,logger Object
logger.setLevel(logging.DEBUG)  # set log level

formatter = logging.Formatter('%(asctime)s:%(levelname)s:%(name)s:%(message)s')  # getting formatter

file_handler = logging.FileHandler('logFile')  # getting file handler
file_handler.setLevel(logging.DEBUG)
file_handler.setFormatter(formatter)  # set formatter in file handler

stream_handler = logging.StreamHandler() # add stream handler to show messages in console
stream_handler.setFormatter(formatter)

logger.addHandler(file_handler)
logger.addHandler(stream_handler)

class AgendaHandler(object):
    """
    classdocs

    """

    def __init__(self, ):
        """
        Constructor
        """
        self.name = ""
        self.age = ""
        self.country = ""
        self.mail = ""
        self.directory = []
        self.strDirectory = ""
        self.index = 0
        self.index2 = 0
        self.x = 0
        self.y = 0

    def load_agenda(self):

        try:
            f = open("../main/agenda.txt", "x")

            self.strDirectory = str(f.read())
            self.directory = self.strDirectory.split("]")
            logger.info("directory loaded")
            f.close()

        except IOError:
            print("Exception occur File could not be opened")
            logger.error("open file exception in load_agenda method", exc_info=True)

        return self

    def save_agenda(self, directory):

        self.directory = directory
        try:
            f = open("../main/agenda.txt", "a")
            f.truncate(0)
            f.write(str(self.directory))
            f.close()
            logger.info("file saved")

        except IOError:
            print("Exception occur File could not be opened")
            logger.error("open file exception in save_agenda method", exc_info=True)

        f = open("../main/agenda.txt", "r")
        logger.info("the file contain: " + f.read())  # Logging message, instead of print function
        f.close()

        return True

    def add_new_record(self, name, age, country, mail):

        self.name = name
        self.age = age
        self.country = country
        self.mail = mail

        self.directory.append(str(self.name) + "," + str(self.age) + "," + str(self.country) + "," + str(self.mail))
        logger.info("new record added")

    def delete_record(self, mail, age):

        self.mail = mail
        self.age = age

        self.index = [n for n, x in enumerate(self.directory) if self.mail in x]
        self.index2 = [n for n, y in enumerate(self.directory) if self.age in y]

        if int(self.index[0]) == int(self.index2[0]):
            self.directory.pop(int(self.index[0]))
            logger.info("record deleted")
            return True
        else:
            return False

    def search_record(self, mail, age):

        self.mail = mail
        self.age = age

        self.index = [n for n, x in enumerate(self.directory) if self.mail in x]
        self.index2 = [n for n, y in enumerate(self.directory) if self.age in y]

        if self.index == self.index2:
            logger.info("record fined")
            return True
        else:
            logger.info("record not fined")
            return False

    def print_records(self, directory):

        # self.directory=directory

        print(self.directory)
        logger.info("record printed")
        return self.directory
